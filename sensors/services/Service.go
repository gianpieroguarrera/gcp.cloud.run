package services

import (
	logger "gcp.cloud.run/LogUtility"

	validator "gcp.cloud.run/validator"

	repository "gcp.cloud.run/repository"

	httpStatus "net/http"

	gin "github.com/gin-gonic/gin"

	Entity "gcp.cloud.run/model"
)

var Sensors = []Entity.Sensor{
	{Id: 1, Name: "sensore_1", Date: "25-10-2021"},
	{Id: 2, Name: "sensore_2", Date: "25-10-2021"},
}

func CreateInfoSensor() {
	Sensors[0].InfoSensor = map[string]string{"asse_x": "1000", "asse_y": "2000", "nomenclatura": "GOLANG"}
	Sensors[1].InfoSensor = map[string]string{"asse_x": "2000", "asse_y": "1500", "nomenclatura": "GOLANG_2"}
}

// GET method
func GetInfo(c *gin.Context) {
	logger.LogStart()
	c.IndentedJSON(httpStatus.StatusOK, Sensors)
	logger.LogEnd()

}

// POST method
func PostInfo(c *gin.Context) {
	logger.LogStart()
	//validator
	err := validator.ValidateBodyAndAddInSlice(*c, &Sensors)
	//return response
	if err == nil {
		c.IndentedJSON(httpStatus.StatusCreated, "Created")
	} else {
		c.IndentedJSON(httpStatus.StatusBadRequest, err.Error())
	}

	logger.LogEnd()
}

// POST method
func PutInfo(c *gin.Context) {
	logger.LogStart()
	//validator
	err := validator.ValidateBodyAndUpdate(*c, &Sensors)
	//return response
	if err == nil {
		c.IndentedJSON(httpStatus.StatusCreated, "Created")
	} else {
		c.IndentedJSON(httpStatus.StatusBadRequest, err.Error())
	}

	logger.LogEnd()
}

//Get id method
func GetId(c *gin.Context) {
	logger.LogStart()
	result, err := repository.FindByID(c.Param("id"), Sensors)
	if err != nil {
		c.IndentedJSON(httpStatus.StatusNotFound, "NOT FOUND")
	} else {
		c.IndentedJSON(httpStatus.StatusOK, result)
	}
	logger.LogEnd()
}

//Get id method
func DeleteByID(c *gin.Context) {
	logger.LogStart()
	result, err := repository.DeleteByID(c.Param("id"), &Sensors)
	if err != nil {
		c.IndentedJSON(httpStatus.StatusNotFound, "NOT FOUND")
	} else {
		c.IndentedJSON(httpStatus.StatusOK, result)
	}
	logger.LogEnd()
}
