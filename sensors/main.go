package main

import (
	Service "gcp.cloud.run/services"
	"github.com/gin-gonic/gin"
)

func main() {
	Service.CreateInfoSensor()

	router := gin.Default()

	//definition GET all
	router.GET("/info", Service.GetInfo)

	//definition POST document
	router.POST("/post", Service.PostInfo)

	//definition POST document
	router.PUT("/update", Service.PutInfo)

	//definition GET single element
	router.GET("/info/:id", Service.GetId)

	//definition DElETE single element
	router.DELETE("/delete/:id", Service.DeleteByID)

	//start the Gin Engine at port 8070
	router.Run("localhost:8070")
}
