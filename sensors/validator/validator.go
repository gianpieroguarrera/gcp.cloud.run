package Validator

import (
	"fmt"
	logs "gcp.cloud.run/LogUtility"
	Entity "gcp.cloud.run/model"

	"github.com/gin-gonic/gin"
)

//check if the input json has the right type and if not exist in the slice
func ValidateBodyAndAddInSlice(c gin.Context, savedDocs *[]Entity.Sensor) error {
	newEntity, err := convertBodyInJSON(c)
	if err != nil {
		return err
	}

	//validate index
	err = checkifDuplicates(*savedDocs, newEntity)
	if err != nil {
		return err
	}

	//add element in the slice
	*savedDocs = append(*savedDocs, newEntity)
	return nil

}

//check if the input json has the right type and if exist in the array
func ValidateBodyAndUpdate(c gin.Context, savedDocs *[]Entity.Sensor) error {
	newEntity, err := convertBodyInJSON(c)
	if err != nil {
		return err
	}

	errUpdate, done := update(*savedDocs, newEntity)

	if done {
		return nil
	} else {
		logs.LogOperation(errUpdate)
		return errUpdate
	}
}

func update(savedDocs []Entity.Sensor, newEntity Entity.Sensor) (error, bool) {
	for index, singleElement := range savedDocs {
		if singleElement.Id == newEntity.Id {
			savedDocs[index] = newEntity
			return nil, true
		}
	}
	return fmt.Errorf(fmt.Sprintf("%v not exist", newEntity)), false
}

//check if the elements exists in the slice, if exist throw error
func checkifDuplicates(savedDocs []Entity.Sensor, newEntity Entity.Sensor) error {
	for index, singleElement := range savedDocs {
		if singleElement.Id == newEntity.Id {
			newError := fmt.Errorf(fmt.Sprintf("%v already exist at index %v", newEntity, index))
			logs.LogOperation(newError)
			return newError
		}
	}
	return nil
}

func convertBodyInJSON(c gin.Context) (Entity.Sensor, error) {
	var newEntity Entity.Sensor

	//validate json type
	err := c.BindJSON(&newEntity)
	return newEntity, err
}
