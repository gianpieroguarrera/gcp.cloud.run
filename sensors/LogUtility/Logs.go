package LogUtility

import (
	logger "log"
)

func LogStart() {
	logger.SetPrefix("INFO LOGS: ")
	logger.Printf("start")
}

func LogEnd() {
	logger.SetPrefix("INFO LOGS: ")
	logger.Printf("end")
}

func LogOperation(err error) {
	logger.SetPrefix("ERR LOGS: ")
	logger.Println(err.Error())
}
