package repository

import (
	"errors"
	stringConverter "strconv"

	Entity "gcp.cloud.run/model"
)

func FindByID(id string, entities []Entity.Sensor) (Entity.Sensor, error) {
	identifier, _ := stringConverter.Atoi(id)
	for index, entity := range entities {
		if entity.Id == int32(identifier) {
			return entities[index], nil
		}
	}
	return Entity.Sensor{Id: -1, Name: "NOT FOUND"}, errors.New("Not FOUND")
}

func DeleteByID(id string, sensors *[]Entity.Sensor) (*[]Entity.Sensor, error) {
	sensorsToOperate := *sensors
	identifier, _ := stringConverter.Atoi(id)
	for index, sensor := range *sensors {
		if sensor.Id == int32(identifier) {
			sensorsToOperate = append(sensorsToOperate[:index], sensorsToOperate[index+1:]...)
			*sensors = sensorsToOperate
		}
	}
	return sensors, nil
}
