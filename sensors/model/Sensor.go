package model

type Sensor struct {
	Id         int32             "json: identifier"
	Name       string            "json: name"
	Date       string            "json: date"
	InfoSensor map[string]string "json: info_sensor"
}
