package Validator

import (
	"fmt"

	logs "gcp.cloud.run/LogUtility"
	Entity "gcp.cloud.run/model"

	"github.com/gin-gonic/gin"
)

//check if the input json has the right type and if not exist in the slice
func ValidateBodyAndAddInSlice(c gin.Context) (Entity.User, error) {
	newEntity, err := convertBodyInJSON(c)
	if err != nil {
		return newEntity, err
	}

	return newEntity, nil

}

//check if the input json has the right type and if exist in the array
func ValidateBodyAndUpdate(c gin.Context, savedDocs *[]Entity.User) error {
	newEntity, err := convertBodyInJSON(c)
	if err != nil {
		return err
	}

	errUpdate, done := update(*savedDocs, newEntity)

	if done {
		return nil
	} else {
		logs.LogOperationError(errUpdate)
		return errUpdate
	}
}

func update(savedDocs []Entity.User, newEntity Entity.User) (error, bool) {
	for index, singleElement := range savedDocs {
		if singleElement.Id == newEntity.Id {
			savedDocs[index] = newEntity
			return nil, true
		}
	}
	return fmt.Errorf(fmt.Sprintf("%v not exist", newEntity)), false
}

func convertBodyInJSON(c gin.Context) (Entity.User, error) {
	var newEntity Entity.User

	//validate json type
	err := c.BindJSON(&newEntity)
	return newEntity, err
}
