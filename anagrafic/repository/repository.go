package repository

import (
	"context"
	"os"
	"time"

	logger "gcp.cloud.run/LogUtility"
	Entity "gcp.cloud.run/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func SaveOnMongo(user Entity.User) error {
	client := mongoConnect()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := returnCollection(client)
	_, err := collection.InsertOne(ctx, user)
	if err != nil {
		logger.LogOperationError(err)
	}
	mongoDisconnect(client)
	return err
}

func Update(user Entity.User) error {
	client := mongoConnect()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := returnCollection(client)
	_, err := collection.ReplaceOne(ctx, bson.M{"id": user.Id}, user)
	if err != nil {
		logger.LogOperationError(err)
	}
	mongoDisconnect(client)
	return err
}

func FindAll() ([]Entity.User, error) {
	var user []Entity.User
	client := mongoConnect()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := returnCollection(client)
	findResult, err := collection.Find(ctx, bson.M{})
	mongoDisconnect(client)
	if err == nil {
		err = findResult.All(ctx, &user)
	}
	return user, err
}

func FindByID(id int32) (Entity.User, error) {
	client := mongoConnect()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := returnCollection(client)
	findResult := collection.FindOne(ctx, bson.M{"id": id})
	mongoDisconnect(client)
	var user Entity.User
	err := findResult.Decode(&user)
	return user, err
}

func DeleteByID(id int32) (int64, error) {
	client := mongoConnect()
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	collection := returnCollection(client)
	deleteResult, err := collection.DeleteOne(ctx, bson.M{"id": id})
	if err != nil {
		logger.LogOperationError(err)
	}
	mongoDisconnect(client)
	return deleteResult.DeletedCount, err
}

func mongoDisconnect(client *mongo.Client) {
	logger.LogOperation("disconnecting from mongo")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client.Disconnect(ctx)
	logger.LogOperation("disconnected from mongo")
}

func returnCollection(client *mongo.Client) *mongo.Collection {
	db := client.Database(os.Getenv("database"))
	collection := db.Collection(os.Getenv("collection"))
	return collection
}

func mongoConnect() *mongo.Client {
	logger.LogOperation("Etablishing connection to mongo")
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("uri")))
	if err != nil {
		logger.LogOperationError(err)
	}
	logger.LogOperation("Established connection to mongo")
	return client
}
