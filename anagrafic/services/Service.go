package services

import (
	"strconv"

	logger "gcp.cloud.run/LogUtility"

	validator "gcp.cloud.run/validator"

	repository "gcp.cloud.run/repository"

	httpStatus "net/http"

	gin "github.com/gin-gonic/gin"

	Entity "gcp.cloud.run/model"
)

var Users = []Entity.User{
	{Id: 1, Name: "Doriano", Surname: "Rossi", Address: "Via Sparano, 1", City: "Bari", Email: "doriano.rossi@spindox.it"},
	{Id: 2, Name: "Mario", Surname: "Bianchi", Address: "Via Corso, 10", City: "Bari", Email: "mario.bianchi@spindox.it"},
	{Id: 3, Name: "Tiziano", Surname: "Monti", Address: "Via Emanuele, 100", City: "Roma", Email: "tiziano.monti@spindox.it"},
	{Id: 4, Name: "Giovanni", Surname: "Sperti", Address: "Corso Trapani, 200", City: "Trieste", Email: "giovanni.sperti@spindox.it"},
}

// GET method
func GetInfo(c *gin.Context) {
	logger.LogStart()
	result, err := repository.FindAll()
	if err == nil {
		c.JSON(httpStatus.StatusOK, gin.H{
			"code":     httpStatus.StatusOK,
			"document": result,
		})
	} else {
		c.JSON(httpStatus.StatusNotFound, gin.H{
			"code":  httpStatus.StatusNotFound,
			"error": string(err.Error()),
		})
	}
	logger.LogEnd()
}

// POST method
func PostInfo(c *gin.Context) {
	logger.LogStart()
	//validator
	newEntity, err := validator.ValidateBodyAndAddInSlice(*c)
	if err == nil {
		err = repository.SaveOnMongo(newEntity)
	}
	//return response
	if err == nil {
		c.JSON(httpStatus.StatusCreated, gin.H{
			"code":     httpStatus.StatusCreated,
			"document": newEntity,
		})
	} else {
		c.JSON(httpStatus.StatusBadRequest, gin.H{
			"code":     httpStatus.StatusBadRequest,
			"error":    string(err.Error()),
			"document": newEntity,
		})
	}

	logger.LogEnd()
}

// UPDATE method
func PutInfo(c *gin.Context) {
	logger.LogStart()
	//validator
	newEntity, err := validator.ValidateBodyAndAddInSlice(*c)
	if err == nil {
		err = repository.Update(newEntity)
	}
	//return response
	if err == nil {
		c.JSON(httpStatus.StatusCreated, gin.H{
			"code":     httpStatus.StatusCreated,
			"document": newEntity,
		})
	} else {
		c.JSON(httpStatus.StatusBadRequest, gin.H{
			"code":     httpStatus.StatusBadRequest,
			"error":    string(err.Error()),
			"document": newEntity,
		})
	}

	logger.LogEnd()
}

//Get id method
func GetId(c *gin.Context) {
	logger.LogStart()
	id, _ := strconv.Atoi(c.Param("id"))
	result, err := repository.FindByID(int32(id))
	if err == nil {
		c.JSON(httpStatus.StatusOK, gin.H{
			"code":     httpStatus.StatusOK,
			"document": result,
		})
	} else {
		c.JSON(httpStatus.StatusNotFound, gin.H{
			"code":  httpStatus.StatusNotFound,
			"error": string(err.Error()),
		})
	}
	logger.LogEnd()
}

//Get id method
func DeleteByID(c *gin.Context) {
	logger.LogStart()
	id, _ := strconv.Atoi(c.Param("id"))
	deletedElement, err := repository.DeleteByID(int32(id))
	//return response
	if err == nil {
		c.JSON(httpStatus.StatusOK, gin.H{
			"code":             httpStatus.StatusOK,
			"deleted elements": deletedElement,
		})
	} else {
		c.JSON(httpStatus.StatusNotFound, gin.H{
			"code":  httpStatus.StatusNotFound,
			"error": string(err.Error()),
		})
	}

	logger.LogEnd()
}
