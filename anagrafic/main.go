package main

import (
	"os"

	logger "gcp.cloud.run/LogUtility"
	Service "gcp.cloud.run/services"
	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

func main() {

	err := godotenv.Load(".env")

	if err != nil {
		logger.LogOperationError(err)
	}
	router := gin.Default()

	//definition GET all
	router.GET("/info", Service.GetInfo)

	//definition POST document
	router.POST("/post", Service.PostInfo)

	//definition POST document
	router.PUT("/update", Service.PutInfo)

	//definition GET single element
	router.GET("/info/:id", Service.GetId)

	//definition DElETE single element
	router.DELETE("/delete/:id", Service.DeleteByID)

	//start the Gin Engine at port 8070

	port := os.Getenv("PORT")
	router.Run(":" + port)
}
