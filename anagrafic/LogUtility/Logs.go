package LogUtility

import (
	logger "log"
)

func LogStart() {
	logger.SetPrefix("INFO LOGS: ")
	logger.Printf("start")
}

func LogEnd() {
	logger.SetPrefix("INFO LOGS: ")
	logger.Printf("end")
}

func LogOperationError(err error) {
	logger.SetPrefix("ERR LOGS: ")
	logger.Println(err.Error())
}
func LogOperation(message string) {
	logger.SetPrefix("INFO LOGS: ")
	logger.Println(message)
}
