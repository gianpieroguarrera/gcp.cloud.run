package model

type User struct {
	Id      int32  "json:id"
	Name    string "json:name"
	Surname string "json:surname"
	Address string "json:address"
	City    string "json:city"
	Email   string "json:email"
}
